//#include <RcppGSL.h>
#include <RcppArmadillo.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_histogram2d.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_matrix.h>
//#include <iostream>

// [[Rcpp::depends(RcppArmadillo, RcppGSL)]]

// [[Rcpp::export]]
arma::vec hist_1d(arma::vec v1, int nbins, bool probs) {
  
  gsl_histogram * h = gsl_histogram_alloc (nbins);
  gsl_histogram_set_ranges_uniform (h, v1.min(), v1.max() + 0.0000001);
  
  for(int x = 0; x < v1.n_elem; x++){
    gsl_histogram_increment (h, v1[x]);
  }
  
  //gsl_histogram_fprintf (stdout, h, "%g", "%g");
  
  //Create a vector that will contains the resulting histogram
  arma::vec res(nbins);
  
  for(int x = 0; x < res.n_elem; x++){
    res[x] = gsl_histogram_get(h, x);
  }
  
  // Free the memory
  gsl_histogram_free(h);
  
  // If the user wants the probability matrix instead of the count matrix
  if(probs){
    res = res / v1.n_elem;
  }
    
  return(res);  
}

// [[Rcpp::export]]
arma::mat hist_2d(arma::vec v1, arma::vec v2, int nbins, bool probs) {
  
  gsl_histogram2d * h = gsl_histogram2d_alloc (nbins, nbins);
  gsl_histogram2d_set_ranges_uniform (h, v1.min(), v1.max() + 0.001, v2.min(), v2.max() + 0.001);
  
  for(int x = 0; x < v1.n_elem; x++){
    gsl_histogram2d_increment (h, v1[x], v2[x]);
  }
  
  //gsl_histogram2d_fprintf (stdout, h, "%g", "%g");
  
  //Create a matrix that will contains the resulting histogram  
  arma::mat res = arma::zeros<arma::mat>(nbins,nbins);
  
  for(int line = 0; line < nbins; line++){
    for(int col = 0; col < nbins; col++){
      res(line, col) = gsl_histogram2d_get(h, line, col);    
    }
  }
  
  // If the user wants the probability matrix instead of the count matrix
  if(probs){
    res = res / v1.n_elem;
  }
  
  // Free the memory
  gsl_histogram2d_free(h);
  
  return(res);
}

