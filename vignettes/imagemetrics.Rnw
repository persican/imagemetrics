\documentclass{article}

\usepackage{natbib}
\usepackage{graphics}
\usepackage{amsmath}
\usepackage{indentfirst}
\usepackage[utf8]{inputenc}
\usepackage[margin = 1.5cm]{geometry}
\setlength\parindent{0pt}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}

 
\begin{filecontents*}{references.bib}
@Manual{knitr2013,
    title = {knitr: A general-purpose package for dynamic report
      generation in R},
    author = {Yihui Xie},
    year = {2013},
    note = {R package version 1.4.1},
    url = {http://yihui.name/knitr/},
}
  
@article{Mellin2012a,
author = {Mellin, Camille and Parrott, Lael and Andr\'{e}fou\"{e}t, Serge and Bradshaw, Corey J.A. a and MacNeil, M Aaron and Caley, M Julian},
issn = {1051-0761},
journal = {Ecological Applications},
month = apr,
number = {3},
pages = {792--803},
pmid = {22645811},
title = {{Multi-scale marine biodiversity patterns inferred efficiently from habitat image processing}},
volume = {22},
year = {2012}
}

@book{Petrou2006,
address = {Chichester ; Hoboken, NJ},
annote = {GLADN152056941
Maria Petrou, Pedro Garc\'{\i}a Sevilla.
Dealing with texture.
ill. ; 26 cm.
Includes bibliographical references and index.},
author = {Petrou, Maria and Sevilla, PG and Wiley, J},
isbn = {0470026286 9780470026281},
keywords = {Image processing Digital techniques.},
pages = {618},
publisher = {John Wiley \& Sons Inc.},
title = {{Image processing: dealing with texture}},
url = {http://www.lavoisier.fr/livre/notice.asp?id=OKLWOOA3X3SOWD},
year = {2006}
}

@article{Proulx2008,
author = {Proulx, R and Parrott, L},
issn = {1470-160X},
journal = {Ecological Indicators},
keywords = {ecological orientor structural complexity imagery},
pages = {270--284},
title = {{Measures of structural complexity in digital images for monitoring the ecological signature of an old-growth forest ecosystem}},
url = {<Go to ISI>://000252688600008 http://www.sciencedirect.com/science/article/pii/S1470160X07000295},
volume = {8},
year = {2008}
}

@article{Meyer2008,
author = {Meyer, George E. and Neto, Joao Camargo},
doi = {10.1016/j.compag.2008.03.009},
issn = {01681699},
journal = {Computers and Electronics in Agriculture},
number = {2},
pages = {282--293},
title = {{Verification of color vegetation indices for automated crop imaging applications}},
volume = {63},
year = {2008}
}


\end{filecontents*}


<<setup, echo=FALSE, message=FALSE,warning=FALSE>>=
library(imagemetrics)
library(raster)

library(knitr)
options(width=50)
opts_chunk$set(tidy = TRUE)
@


%\VignetteIndexEntry{Tutorial}

\begin{document}


\title{Image metrics: A tutorial}
\author{Philippe Massicotte}
\maketitle

\section*{}
In this short tutorial I describe how to use the \texttt{imagemetrics} package.

\subsection*{Step 1: Install the package}

The package is currently hosted on \href{https://bitbucket.org/persican/imagemetrics}{Bitbucket}
and can be installed using \href{http://cran.r-project.org/web/packages/devtools/index.html}{devtools}.


<<message = FALSE, eval=FALSE>>=
library(devtools)

install_bitbucket("imagemetrics", "persican")
library(imagemetrics)
@

\subsection*{Step 2: Open a raster image}

Indeed, the first thing to do is to open the image on which you want to calculate the metrics.

<<r chunk_9b, fig.width=4, fig.height=4, fig.align='center', eval=TRUE>>=
## Open the R logo and average on calculate the average on R,G,B channels
r <- brick(system.file("external/rlogo.grd", package = "raster"))
r <- mean(r)

## Plot the raster (optional)
plot(r, useRaster = FALSE, col  = gray((0:100)/100))
@

\subsection*{Step 3: Extract pixels from the image to calculate occurrence probabilities}

It is worth mentioning that the metrics are calculated on a probability matrix that represents the chances of getting a specific pair of pixel values. For each pixel in the image, we have to choose a neighbor that is located either to the right, bottom right or bottom of a reference pixel (see the following image).

\begin{figure}[h!]
\centering
\includegraphics[scale=1.5]{pixels}
\caption{Representation of three choices to choose the neighbor.}
\end{figure}

Before calculating such probabilities, we have to extract the values of both reference and neighbor pixels. To do so, the user can use \verb|getImagePixels(r, side)|. The function takes as parameters a raster image (\verb|r|) and \verb|side|, a numeric value specifying the neighbor to use. \verb|side = 1| for the right pixel, \verb|side = 2| for the lower right pixel, \verb|side = 3| the bottom pixel.

<<r chunk_9c>>=
## Get bottom pixel (side = 3)
v <- getImagePixels(r, side = 3)

str(v)
@

\subsection*{Step 4: Calculate occurrence probabilities}

To calculate the probabilities, simply use \verb|calculateHisto(reference_vector, neighbour_vector, nbins)|, where \verb|reference_vector| and \verb|neighbour_vector| are vectors returned by \verb|getImagePixels| and \verb|nbins| is a numerical value indicating the bin size used to compute histograms.\\  

From \cite{Mellin2012a}: 

\quote{For $M$ classes of values, the number of possible configurations in a $k-pixel$ neighborhood is $M^k$. To ensure that each possible configuration has a reasonable probability of occurring in an image, it is generally recommended that the ratio of the total number of pixels in the image to $M^k$ be greater than 100.}\\

\noindent\verb%suggestMaximumBins(r)% can be used to determine the number of maximum bins to use.

<<r chunk_9d>>=
maxbins <- suggestMaximumBins(r)
maxbins

## Calculate probability 
r.prob <- calculateHisto(reference_vector = v$reference_vector, neighbour_vector = v$neighbour_vector, nbins = maxbins)
@

\subsection*{Step 5: Compute the metrics}

Metrics are thereafter calculated using the object returned by \verb%calculateHisto%.

<<r chunk_9e, dev = 'svg', warning = FALSE, message = FALSE>>=
contagion(r.prob)
contrast(r.prob)
evenness(r.prob)
homogeneity(r.prob)
jointEntropy(r.prob)
marginalEntropy(r.prob)
maximumMutualInformation(r.prob)
meanInformationGain(r.prob)
@

\section*{Metric definitions}
The equations used in this package are from \cite{Mellin2012a}, \cite{Petrou2006} and \cite{Proulx2008}.

\subsection*{Evenness}
\begin{equation}
evenness = \frac{marginalEntropy}{log(nbins)}
\end{equation}

\subsection*{Marginal entropy}
\begin{equation}
marginalEntropy = -\sum\limits_{m=0}^{nbins-1} p(m)log(p(m))
\end{equation}

\subsection*{Contagion}
\begin{equation}
contagion = 1-\frac{jointEntropy}{2log(nbins)}
\end{equation}

\subsection*{Joint entropy}
\begin{equation}
jointEntropy = -\sum\limits_{m=0}^{nbins-1}\sum\limits_{n=0}^{nbins-1} p(m,n)log(p(m,n))
\end{equation}

\subsection*{Contrast}
\begin{equation}
contrast = \frac{1}{(nbins-1)^2}\sum\limits_{m=0}^{nbins-1}\sum\limits_{n=0}^{nbins-1} (m-n)^2p(m,n)
\end{equation}

\subsection*{Homogeneity}
\begin{equation}
homogeneity = \sum\limits_{m=0}^{nbins-1}\sum\limits_{n=0}^{nbins-1} \frac{p(m,n)}{1 + |m-n|}
\end{equation}

\subsection*{Mean information gain}
\begin{equation}
homogeneity = 2-2contagion-evenness
\end{equation}

\subsection*{Maximum mutual information}
\begin{equation}
maximumMutualInformation = 2contagion+2evenness-2
\end{equation}

\newpage
\section*{getBinaryVegetationMask}
The function returns a binary mask (0/1) representing the abscence/presence of vegetation on a digital image \citep{Meyer2008}.

<<mask, message=FALSE, eval=TRUE, out.width='6cm', out.height='6cm',  fig.show='hold', fig.align='center'>>=

X = brick(paste(system.file("extdata", package = "imagemetrics"), "/plant.jpg", sep = ""))

X_binary = getBinaryVegetationMask(X)

plotRGB(X)
plot(X_binary, col= colorRampPalette(c("black", "white"))(2), legend = F,  axes = FALSE, box = F, asp = 1)
@

<<compact, message=FALSE,warning=FALSE, echo=FALSE>>=
tools::compactPDF(paste(system.file("doc", package = "imagemetrics"), "/figure/mask1.pdf", sep = ""), gs_quality = "ebook")
tools::compactPDF(paste(system.file("doc", package = "imagemetrics"), "/figure/mask2.pdf", sep = ""), gs_quality = "ebook")
@


\newpage
% ----------------
% References
% ----------------

\bibliographystyle{apalike}
\bibliography{references}

\end{document}

