\encoding{UTF-8}
\name{getBinaryVegetationMask}
\alias{getBinaryVegetationMask}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Get binary vegetation mask
}
\description{
The function returns a binary mask (0/1) representing the abscence/presence of vegetation on a digital image.
}
\usage{
getBinaryVegetationMask(X)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{X}{
A \code{\link{raster}} image.
}
}
\value{
A binary \code{\link{raster}} mask. 
}
\references{
Meyer, G.E. & Neto, J.C. (2008) Verification of color vegetation indices for automated crop imaging applications. Computers and Electronics in Agriculture, 63, 282–293.
}
\author{
\email{philippe.massicotte@uqtr.ca}.
}
\examples{
\dontrun{
X = brick("myimage.jpg")

X_binary = getBinaryVegetationMask(X)

par(mfrow = c(1,2))
plotRGB(X)
plot(X_binary, col= colorRampPalette(c("black", "white"))(2), legend = F,  axes = FALSE, box = F)
}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
