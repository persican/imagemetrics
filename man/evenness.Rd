\encoding{UTF-8}
\name{evenness}
\alias{evenness}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Calculate the evenness metric.
}
\description{
The function computes the evenness metric (see details).}
\usage{
evenness(prob)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{prob}{
%%     ~~Describe \code{hist1d} here~~
An object returned by \code{\link{calculateHisto}}.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
\deqn{evenness = marginalEntropy / log(nbins)}{sdsdf}
}
\value{
A \code{numeric} value from the equation shown in the details section.
}
\references{
%% ~put references to the literature/web site here ~
Mellin, C., Parrott, L., Andréfouët, S., Bradshaw, C.J.A. a, MacNeil, M.A. & Caley, M.J. (2012) Multi-scale marine biodiversity patterns inferred efficiently from habitat image processing. Ecological Applications, 22, 792–803.

Petrou, M., Sevilla, P. & Wiley, J. (2006) Image processing: dealing with texture, John Wiley & Sons Inc., Chichester ; Hoboken, NJ.

Proulx, R. & Parrott, L. (2008) Measures of structural complexity in digital images for monitoring the ecological signature of an old-growth forest ecosystem. Ecological Indicators, 8, 270–284.
}
\author{
\email{philippe.massicotte@uqtr.ca}.
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
\code{\link{calculateHisto}}
}
\examples{
\dontrun{
r = raster(system.file("external/rlogo.grd", package = "raster"), band = 1) 

## Get bottom pixel (side = 3)
v = getImagePixels(r, side = 3)

## Calculate probability 
prob = calculateHisto(reference_vector = v$reference_vector, neighbour_vector = v$neighbour_vector, nbins = suggestMaximumBins(r))

evenness(prob)

}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
