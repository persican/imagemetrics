#--------------------------------------------------------------------------------------------
## Meyer, G.E. & Neto, J.C. (2008) Verification of color vegetation indices for 
## automated crop imaging applications. Computers and Electronics in Agriculture, 63, 282–293.
#--------------------------------------------------------------------------------------------

getBinaryVegetationMask = function(X){
  
  xx = unstack(X)
  
  ## Calculate ExG and ExR
  hat_R = xx[[1]]/255
  hat_G = xx[[2]]/255
  hat_B = xx[[3]]/255
  
  r = hat_R / (hat_R + hat_G + hat_B)
  g = hat_G / (hat_R + hat_G + hat_B)
  b = hat_B / (hat_R + hat_G + hat_B)
  
  ExG = 2*g - r - b
  ExR = 1.4*r - g
  
  ## Calculate vegetation index
  im_vegetation_index = ExG - ExR
  
  ## Binary image
  im_binary = raster(ncols = ncol(X), nrows = nrow(X))
  extent(im_binary) = extent(X)
  values(im_binary) = ifelse(values(im_vegetation_index) > 0, 1, 0)
  
  ## Make sure we dont have NA or infinite in the mask. Can happen is all pixels (RGB) have a value of 0. 
  im_binary[is.infinite(im_binary)] = 0
  im_binary[is.na(im_binary)] = 0
  
  return(im_binary)
  
}